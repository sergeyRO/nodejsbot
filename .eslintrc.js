module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    'key-spacing': 'off',
    'vue/max-attributes-per-line': 'off',
    'indent': 'off',
    'space-before-blocks': 'off',
    'semi': 'off',
    'keyword-spacing': 'off',
    'quote-props': 'off',
    'comma-dangle': 'off',
    'no-var': 'off',
    'object-curly-spacing': 'off',
    'brace-style': 'off',
    'import/order': 'off',
    'eol-last': 'off',
    'no-multiple-empty-lines': 'off',
    'space-before-function-paren': 'off',
    'arrow-parens': 'off',
    'space-infix-ops': 'off',
    'quotes': 'off',
    'prefer-promise-reject-errors': 'off',
    'handle-callback-err': 'off',
    'no-unused-vars': 'off',
    'func-call-spacing': 'off',
    'space-in-parens': 'off',
    'vue/html-indent': 'off',
    'vue/attributes-order': 'off',
    'vue/no-use-v-if-with-v-for': 'off',
    'new-cap': 'off',
    'no-eval': 'off'
  }
}
