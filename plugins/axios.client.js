export default function({ $axios }) {
    if (process.client) {
      const host = window.location.hostname;
      if (window.location.port !== "") {
        $axios.setBaseURL(window.location.protocol+"//" + host + ":3000");
      } else {
        $axios.setBaseURL(window.location.protocol+"//" + host);
      }
    }
  }