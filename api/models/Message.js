const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Message = new Schema ({
  text: { type: String, required: true },
  chatId: { type: Schema.Types.ObjectId},
  botId: { type: Schema.Types.ObjectId},
  out: {type: Boolean},
  createdon: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Message', Message)