const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Bot = new Schema ({
  name: { type: String, required: true },
  columns: { type: Schema.Types.Mixed, default: []},
  companyId: { type: Schema.Types.ObjectId, required: true },
  createdon: {type: Date, default: Date.now()},
  type: {type: String, required: true}
});

module.exports = mongoose.model('Bot', Bot)