const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Text = new Schema ({
  key: { type: String, default: "" },
  value: { type: String, default: "" },
  index: {type: Number, default: 0},
  botId: { type: Schema.Types.ObjectId, required: true },
  createdon: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Text', Text)