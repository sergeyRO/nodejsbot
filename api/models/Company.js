const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Company = new Schema ({
  name: { type: String, required: true },
  createdon: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Company', Company)