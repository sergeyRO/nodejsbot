const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Chat = new Schema ({
  telegram_id: {type: String},
  name: { type: String},
  botId: { type: Schema.Types.ObjectId},
  answers: { type: Schema.Types.Mixed},
  photo: {type: String},
  createdon: {type: Date, default: Date.now()},
  emoji: {type: [], default: []},
  mood: { type: Schema.Types.Mixed},
  enddate: {type: Date},
  unique: {type: Boolean}
});

Chat.index({'$**': 'text'});

module.exports = mongoose.model('Chat', Chat)