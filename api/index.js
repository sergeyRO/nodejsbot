const express = require('express')

const db = require('./db')

// Create express instance
const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Require API routes
const users = require('./routes/users')
const test = require('./routes/test')
const general = require('./routes/general')

// Import API Routes
app.use(users)
app.use(test)
app.use(general)

// Export express app
module.exports = app

// Start standalone server if directly running
if (require.main === module) {
  const port = process.env.PORT || 3001
  app.listen(port, () => {
    console.log(`API server listening on port ${port}`)
  })
}
