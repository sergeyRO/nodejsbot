const mongoose = require('mongoose');
// mongodb database connection string. change it as per your needs. here "mydb" is the name of the database. You don't need to create DB from mongodb terminal. mongoose create the db automatically.
mongoose.connect('mongodb://izhhr:cua52nmaqu@194.67.92.140:27017/izhhr?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false', {
                            useNewUrlParser: true,
                            useUnifiedTopology: true,
                            useFindAndModify: false,
                            useCreateIndex: true
                          });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("MongoDB Connected...");
});

const Chat = require('./models/Chat');
const Message = require('./models/Message');
const Bot = require('./models/Bot');

async function addSampleData() {
  var chats = await Chat.find({});
  var bots = await Bot.find({});
  console.log('ADD SAMPLE DATA', chats.length);
  chats.forEach(async (chat) => {
    var messages = await Message.find({'chatId': chat._id});
    console.log(messages);
    if (!messages.length) {
      var message1 = new Message({
        text: `Привет! Я - виртуальный помощник HR менеджера розничной сети магазинов Ароматы дома.

        Вы только что отправили свое резюме на нашу вакнсию Продавец- консультант.`,
        chatId: chat._id,
        botId: chat.botId,
        out: true
      });
      var message2 = new Message({
        text: `Это первый этап оценки кандидатов. Ответы получит HR менеджер Иванова Кристина сразу после окончания. Я задам 6- 10 вопросов. Это займет не более 15 минут. Нажмите на кнопку ниже, когда будете готовы начать:`,
        chatId: chat._id,
        botId: chat.botId,
        out: true
      })
      var message3 = new Message({
        text: `Готов`,
        chatId: chat._id,
        botId: chat.botId,
        out: false
      })

      await message1.save();
      await message2.save();
      await message3.save();

      console.log('SAVED MESSAGES', chat._id);
    }
  })
}
// addSampleData();


module.exports = db