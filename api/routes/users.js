const config = require('../config')

const { Router } = require('express')

const router = Router();

const User = require('../models/User');

// Initialize Controller
const usersController = require('../controllers/usersController')

// Register
router.post('/users/register', usersController.register)

// Login
router.post('/users/login', usersController.login)

// Get User
router.get('/users/user', usersController.user)

router.get('/users/list', usersController.usersGet)

router.get('/users/remove', async (req, res) => {
    var userId = req.query.id;

    await User.findByIdAndRemove(userId);

    res.send({'success': true})
})
router.get('/users/activate', async (req, res) => {
    var userId = req.query.id;

    await User.findByIdAndUpdate(userId, {'active': true});

    res.send({'success': true})
})
router.get('/users/deactivate', async (req, res) => {
    var userId = req.query.id;

    await User.findByIdAndUpdate(userId, {'active': false});

    res.send({'success': true})
})

module.exports = router