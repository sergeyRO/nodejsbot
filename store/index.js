export const state = () => ({
    title: "IZH HR"
  })

export const mutations = {
    setTitle(state, title) {
      state.title = title;
    }
}